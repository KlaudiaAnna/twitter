﻿using System;
using System.IO;
using System.Threading.Tasks;
using MongoDB.Driver;
using Newtonsoft.Json;
using TwitterTest.JsonData;
using TwitterTest.MongoDB;
using TwitterTest.Worker;
namespace TwitterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Configurationtwitter app = Configuration.GetConfiguration();
                foreach (TickersInfo ticker in app.Tickers)
                {
                    /* for Tickers */
                    Request request = new Request(ticker.Name);
                    string download = request.Download();
                    Tweetdata tw = JsonConvert.DeserializeObject<Tweetdata>(download);
                    ParsingType.Date_ID_Parse(tw);
                    CountWordinTweet.Words(tw);
                    Contains c = new Contains(ticker.Name);
                    c.TickerOnlyOne(tw);
                    SaveinTXT save = new SaveinTXT(ticker.Name);
                    save.SavetoTXT(tw);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error :" + ex.Message);
            }
        }
        private static void DisplayDatabaseNames()
        {
            try
            {
                MongoClient client = new MongoClient();
                var database = client.GetDatabase("TwitterDB");
                var collection = database.GetCollection<InformationTweet>("InformationTweet");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error :" + ex.Message);
            }
        }
    }
}
