﻿namespace TwitterTest
{
   public class ContainTweetInfo
    {
        public ContainTweetInfo() { }
        public string AuthorTweet { get; set; }
        public string Text { get; set; }
        public string PublishedTweet { get; set; }
        public string Date { get; set; }
    }
}
