﻿using System.Collections.Generic;

namespace TwitterTest
{
    public class Configurationtwitter
    {
        public Configurationtwitter()
        { }
        public string ApiUrl { get; set; }
        public string ApiKey { get; set; }
        public string ApiSecretKey { get; set;}
        public string TokenKey { get; set; }
        public string TokenSecretKey { get; set; }
        public string ConnectionString { get; set;}
        public int CounterTweet { get; set; }
        public int CounterWords { get; set; }

        public List<TickersInfo> Tickers { get; set; }
    }
    public class TickersInfo
    {
        public TickersInfo()
        { }
        public string Name { get; set; }
    }
}