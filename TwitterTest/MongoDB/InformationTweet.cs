﻿namespace TwitterTest.MongoDB
{
     class InformationTweet
    {
        public int Id { get; set; }
        public string Ticker { get; set; }
        public int NumberofWordsibnTweet { get; set; }
        public string User { get; set; }
        public int UserID { get; set; }
        public string PublishedTweetDate { get; set; }
        public string ContentofTweet { get; set; }
        public int ContentofTweetID { get; set; }
        public int NeuralNetworkRating { get; set; }
        public int HumanRating { get; set; }
    }
}
