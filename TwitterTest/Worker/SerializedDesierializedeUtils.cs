﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
namespace TwitterTest
{
    static class SerializedDesierializedeUtils
    {
        /*  SerializeToXml */

        public static string

            SerializeToXml<T>(this T value)
        {
            string xml = string.Empty;
            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, value);
                    xml = sww.ToString();
                }
            }
            return xml;
        }
        /* Deserialize  */
        public static T Deserialize<T>(string config)
        {
            T returnObject = default(T);
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));

                using (StringReader sr = new StringReader(config))
                {
                    returnObject = (T)serializer.Deserialize(sr);
                }
                   }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnObject;
        }
    }
}
