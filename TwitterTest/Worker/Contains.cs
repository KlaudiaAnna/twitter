﻿using TwitterTest.JsonData;
namespace TwitterTest.Worker
{
    public class Contains
    {
        public string ticker = string.Empty;
        public Contains(string ticker)
        {
            this.ticker = ticker;
        }
        public void TickerOnlyOne(Tweetdata tweetdata)
        {
            for (int i = tweetdata.Statuses.Count - 1; i >= 0; i--)
            {
                var symbol = tweetdata.Statuses[i].Entities.Symbols;
                for (var j = symbol.Length - 1 ; j >= 0; j--)
                {
                    if (tweetdata.Statuses[i].Entities.Symbols[j].Text == ticker && symbol.Length == 1)
                    {
                        continue;
                    }
                    else
                    {
                        tweetdata.Statuses.RemoveAt(i);
                    }
                }
            }
        }
    }
}




