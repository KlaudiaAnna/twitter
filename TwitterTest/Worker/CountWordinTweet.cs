﻿using System;
using TwitterTest;
using TwitterTest.JsonData;
public static class CountWordinTweet
{
    public static void Words(Tweetdata tweetdata)
    {
        for (int i = 0; i < tweetdata.Statuses.Count; i++)
        {
            string text = tweetdata.Statuses[i].Text;
            var TweetData = tweetdata.Statuses[i];
            string[] separator = text.Split(' ');
            int wordsCount = text.Split(separator, StringSplitOptions.RemoveEmptyEntries).Length;
            if (wordsCount <= Configuration.GetConfiguration().CounterWords)
            {
                tweetdata.Statuses.RemoveAt(i);
            }
        }
    }
}