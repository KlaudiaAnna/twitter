﻿using System.Xml;
namespace TwitterTest
{
    public static class Configuration
    {
        static Configurationtwitter config;
        public static Configurationtwitter GetConfiguration()
        {
            if (config == null)
            {
                string xml;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load("Config.xml");
                xml = xmlDocument.OuterXml;
                config = SerializedDesierializedeUtils.Deserialize<Configurationtwitter>(xml);
            }
            return config;
        }
    }
}
