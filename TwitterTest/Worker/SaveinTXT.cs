﻿using System;
using System.IO;
using TwitterTest.JsonData;

namespace TwitterTest
{
    public class SaveinTXT
    {
        private string ticker = string.Empty;
        public SaveinTXT(string ticker)
        {
            this.ticker = ticker;
        }
        public void SavetoTXT(Tweetdata tw)
        {
            var t = tw;
            string createText = t + Environment.NewLine;

            using (StreamWriter writetext = new StreamWriter(ticker + ".txt"))
            {
                for (int i = 0; i < t.Statuses.Count; i++)
                {
                    string line = t.Statuses[i].Created + "   " + t.Statuses[i].Id_str + "   " + t.Statuses[i].User.Name + "   " + t.Statuses[i].Text + "   " + Environment.NewLine;
                    writetext.WriteLine(line);
                }
            }
        }
    }
}
