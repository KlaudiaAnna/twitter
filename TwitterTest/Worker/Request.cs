﻿using RestSharp;
using RestSharp.Authenticators;

namespace TwitterTest
{
    public class Request
    {
        private string ticker = string.Empty;
        public Request(string ticker)
        {
            this.ticker = ticker;
        }
        public string Download()
        {
            string settedurl = GetUrl;
            var client = new RestClient(settedurl)
            {
                Authenticator = OAuth1Authenticator.ForProtectedResource(
                       Configuration.GetConfiguration().ApiKey,
                       Configuration.GetConfiguration().ApiSecretKey,
                       Configuration.GetConfiguration().TokenKey,
                       Configuration.GetConfiguration().TokenSecretKey, RestSharp.Authenticators.OAuth.OAuthSignatureMethod.HmacSha1)
            };
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute(request);
            string test = response.Content;
            return test;
        }
        public string url1;
        public string GetUrl
        {  
            get
            {     // %24MSFT%20"%24MSFT"%20%24MSF 
                if (string.IsNullOrEmpty(url1))
                    url1 = Configuration.GetConfiguration().ApiUrl + "%24"+ ticker +  "%20" + ticker + "%20%24" + ticker + "&count=" + Configuration.GetConfiguration().CounterTweet + "&lang=en";
                return url1;
            }
            set
            {
                url1 = value;
            }
        }
    }
}
